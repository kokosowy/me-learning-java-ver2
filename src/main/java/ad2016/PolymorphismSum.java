package ad2016;

/**
 * Created by kokos on 2016-09-05. Javastart.pl
 */
/* NOTE: by the course it was like this but it didn't work
public class ad2016.PolymorphismSum {
    ad2016.PolymorphismX[] tab = new ad2016.PolymorphismX[1];
    tab[0] = new ad2016.PolymorphismX1();

    void someMethod() {
        for (ad2016.PolymorphismX x : tab)
            x.doSomething();
    }
} */
public class PolymorphismSum {
    public static void main (String[] args) {
        PolymorphismX[] tab = new PolymorphismX[1];
        tab[0] = new PolymorphismX1();

        for (PolymorphismX x : tab)
            x.doSomething();
    }
}
