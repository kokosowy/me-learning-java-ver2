package ad2016;

/**
 * Created by kokos on 2016-07-24. Javastart.pl
 * 2.3 Utwórz klasę Punkt, która przechowuje dwie wartości typu int - współrzędne punktu na powierzchni. Napisz w niej
 * także metody które:
 * - zwiększają wybraną współrzędną o 1
 * - zmieniają wybraną zmienną o dowolną wartość
 * - zwracają wartość współrzędnych (oddzielne metody)
 * - wyświetla wartość współrzędnych
 * Napisz także klasę, w której przetestujesz działanie metod wyświetlając działanie metod na ekranie,
 */
public class PointHomework {
    int crX;
    int crY;

    void incX () {
        crX++;
    }

    void incY () {
        crY++;
    }

    void chngX (int i) {
        crX += i;
    }

    void chngY (int i) {
        crY += i;
    }

    int retCrX () {
        return crX;
    }

    int retCrY () {
        return crY;
    }

    void prntXY () {
        System.out.println("("+crX+", "+crY+")");
    }

}
