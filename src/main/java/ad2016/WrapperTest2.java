package ad2016; /**
 * Created by konradal on 14/09/2016. Javastart.pl
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class WrapperTest2 {
    public static void main (String[] args) throws NumberFormatException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("State digit between 0 and 9: ");
        String str = br.readLine();
        int number = 0;
        if ((str.length()==1) && (Character.isDigit(str.charAt(0)))) {
            System.out.println("Sweet, now that's a digit");
            number = Integer.parseInt(str);
        }
        else {
            System.out.println("Hold on! That's not a digit");
        }
    }
}
