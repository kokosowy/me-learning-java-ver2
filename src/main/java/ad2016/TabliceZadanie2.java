package ad2016; /**
 * Created by kokos on 2016-06-28. Javastart.pl
 * 1.13 Utwórz tablicę typu int przechowującą n elementów - gdzie n jest parametrem pobieranym od użytkownika.
 * Następnie wypełnij ją liczbami od 1 do n i wyświetl zawartość na ekranie przy pomocy pętli while.
 */
import java.util.Scanner;

public class TabliceZadanie2 {
    public static void main (String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Podaj liczbę n: ");
        int n = input.nextInt();
        int[] tab = new int[n];

        for (int i=0; i<tab.length; i++) tab[i] = i+1;

        int j = 0;
        while (j<tab.length) {
            System.out.println("Tab["+j+"] = "+tab[j]);
            j++;
        }
    }
}
