package ad2016; /**
 * Created by kokos on 2016-06-27. Javastart.pl
 * 1.11 Napisz program, który pobierze od użytkownika całkowitą liczbę dodatnią. Następnie przy użyciu wyświetl
 * na ekranie Odliczanie z tekstem "Bomba wybuchnie za ... " gdzie w miejsce dwukropka mają się pojawić liczby
 * od podanej przez użytkownika do 0. Napisz program w 3 wersjach przy użyciu różnych pętli.
 */
import java.util.Scanner;

public class PetlaZadanie {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int i = 0;
        do {
            System.out.print("Podaj liczbę i>0: ");
            i = input.nextInt();
            if (i<=0) System.out.println("Nieprawidłowa liczba.");
        }
        while (i<=0);
        int j = i; //zmienna pomocnicza do przechowywania pierwotnej wartości j;

        //pętla while
        while (i>=0) {
            if (i>0) System.out.println("Bomba wybuchnie za "+i);
            else System.out.println("Bum!");
            i--;
        }
        System.out.println();
        //pętla do while
        i = j; //ponowne ustawienie licznika
        do {
            if (i!=0) System.out.println("Bomba wybuchnie za "+i);
            i--;
            if (i==0) System.out.println("Bum!");
        }
        while (i>=0);
        System.out.println();
        //pętla for
        for (i=j; i>=0; i--) {
            if (i!=0) System.out.println("Bomba wybuchnie za "+i);
            else System.out.println("Bum!");
        }
    }
}
