package ad2016;

/**
 * Created by konradal on 25/05/2016.
 * Javastart.pl
 * 1.6 Wykorzystaj konwersję i rzutowanie wypróbuj zamiany różnych typów prostych między sobą. Szczególną uwagę
 * zwróć na rzutowanie char na int. Jak myślisz, co w ten sposób otrzymujesz?
 */
public class KonwersjeZadanie {
    public static void main(String[] args){

        char a = 'a';
        int b = a;
        char c = 101;
        System.out.println(a+" "+b+" "+c);
        System.out.println();

        int d = 25;
        int e = 52;
        char f = (char)(d+e);
        System.out.println(f);
        System.out.println((char)(d-e));
        System.out.println();

        char znak;
        for (int i = 0; i < 256;) {
            ++i;
            znak = (char)i;
            System.out.print(i+" to znak "+znak+", ");
            if (i%10 == 0) {
                System.out.print("\n");
            }
        }

    }
}
