package ad2016;

/**
 * Created by kokos on 2016-08-17. Javastart.pl
 */
 
class FirmaPracownik {
	String imie;
	String nazwisko;
	int wyplata;
	
	public FirmaPracownik () {
		imie = "";
		nazwisko = "";
		wyplata = 0;
	}
	
	public FirmaPracownik (String i, String n, int w) {
		imie = i;
		nazwisko = n;
		wyplata = w;
	}
} 
