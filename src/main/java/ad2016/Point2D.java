package ad2016;

/**
 * Created by konradal on 17/08/2016. Javastart.pl
 * 2.6 Stwórz klasę Punkt2D, która przechowuje informacje na temat punktu na przestrzeni dwuwymiarowej
 * (współrzędne x oraz y). Zawierająca dwa konstruktory: bezparametrowy ustawiający pola na wartość 0, oraz przyjmujący
 * dwa argumenty i ustawiający pola obiektu zgodnie z podanymi parametrami.
 * Napisz klasę Punkt3D dziedziczącą po Punkt2D, reprezentującą punkt w trójwymiarze (dodatkowe pole z).
 * W klasie testowej utwórz obiekty obu klas i przetestuj działanie.
 */
public class Point2D {
    int x;
    int y;

    Point2D () {
        x = 0;
        y = 0;
    }

    //using 'this' - see this again after learning what it exactly does
    Point2D (int x, int y) {
        this.x = x;
        this.y = y;
    }

}
