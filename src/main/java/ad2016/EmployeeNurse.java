package ad2016;

/**
 * Created by kokos on 2016-08-24. Javastart.pl
 */
public class EmployeeNurse extends Employee {
    private int overtime;

    public EmployeeNurse (String name, String surname, double salary) {
        super (name, surname, salary);
        overtime = 0;
    }

    public int getOvertime() { return overtime; }
    public void setOvertime(int n) {
        overtime =+ n;
    }
}
