package ad2016; /**
 * Created by kokos on 2016-05-30.
 * Javastart.pl
 * 1.7 Utwórz dwie zmienne typu double. Następnie przy użyciu klasy Scanner pobierz od użytkownika
 * dwie liczby i wykonaj na nich dodawanie, odejmowanie, mnożenie i dzielenie, wyświetlając wyniki
 * w kolejnych liniach na konsoli.
 */
import java.util.Scanner;

public class WitajZadanie {
    public static void main(String[] args){

        double x, y;

        Scanner odczyt = new Scanner(System.in);
        System.out.print("Podaj liczbe x: ");
        x = odczyt.nextDouble();
        System.out.print("Podaj liczbe y: ");
        y = odczyt.nextDouble();

        System.out.println();
        System.out.println(x+" + "+y+" = "+(x+y));
        System.out.println(x+" - "+y+" = "+(x-y));
        System.out.println(x+" * "+y+" = "+(x*y));
        System.out.println(x+" / "+y+" = "+(x/y));

    }
}
