package ad2016; /**
 * Created by kokos on 2016-08-31. Javastart.pl
 */
 import java.util.Scanner;
 
 public class ReadExceptions2 {
	 public static void main (String[] args) {
		 int tab[] = {1, 2, 3, 4, 5};
		 Scanner input = new Scanner (System.in);
		 int index = -1;
		 
		 System.out.print("Write index of the tab element you would like to see: ");
		 index = input.nextInt();
		 
		 if ((index >= 0) && (index <= tab.length))
			System.out.println("tab["+index+"] = "+tab[index]);
		 else	
			System.out.println("Incorrect value. Table's length is "+tab.length);
	}
} 
