package ad2016; /**
 * Created by konradal on 14/09/2016. Javastart.pl
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class WrapperTest1 {
    public static void main (String[] args) throws NumberFormatException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("State your name: ");
        String imie = br.readLine();

        System.out.print("State your age: ");
        int age = Integer.parseInt(br.readLine());

        //simple test
        System.out.println();
        System.out.println("Thanks, "+imie);
        System.out.println("By the way, did you know that in 15 years, you'll be "+(age+15)+"?");
    }
}
