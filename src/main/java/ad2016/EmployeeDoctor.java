package ad2016;

/**
 * Created by kokos on 2016-08-24. Javastart.pl
 */
public class EmployeeDoctor extends Employee {
    private double bonus;

    public EmployeeDoctor (String name, String surname, double salary) {
        super (name, surname, salary);
        bonus = 0;
    }

    public double getBonus() { return bonus; }
    public void setBonus(double d) {
        bonus =+ d;
    }
}
