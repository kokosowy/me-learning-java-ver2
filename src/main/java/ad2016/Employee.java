package ad2016;

/**
 * Created by kokos on 2016-08-24. Javastart.pl
 */
public class Employee {
    private String name;
    private String surname;
    private double salary;

    public Employee (String name, String surname, double salary) {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    String getName() { return name; }
    String getSurname() { return surname; }
    double getSalary() { return salary; }
}
