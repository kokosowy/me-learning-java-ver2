package ad2016;

/**
 * Created by kokos on 2016-08-09. Javastart.pl
 */
public class PointConstructMain {
    public static void main (String[] args) {
        PointConstruct point1 = new PointConstruct(10, 20);
        System.out.println("Point1's coordinates are:");
        System.out.println("x1: "+point1.x);
        System.out.println("y1: "+point1.y);

        System.out.println();

        PointConstruct point2 = new PointConstruct(point1);
        System.out.println("Point2's coordinates are:");
        System.out.println("x2: "+point2.x);
        System.out.println("y2: "+point2.y);
    }
}
