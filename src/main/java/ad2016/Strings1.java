package ad2016;

/**
 * Created by kokos on 2016-10-09. Javastart.pl
 */
public class Strings1 {
	public static void main (String[] args) {
		String s = "a";
		long start = System.nanoTime();
		for (int i=0; i<10000; i++) {
			s = s+"a";
			//above concatenation is actually:
			//s = new StringBuilder(s).append("a").toString();
		}
		System.out.println("Time1: "+(System.nanoTime()-start));
	}
}
