package ad2016; /**
 * Created by kokos on 2016-07-05.
 * Zadanie: dynamicznie powiększająca się tabela
 */
import java.util.Random;
import java.util.Scanner;

public class GrowingTable {
    public static void main (String[] args) {
        Scanner input = new Scanner(System.in);
        Random digit = new Random();

        System.out.println("Table with randomly generated digits will increase size three (3) times while keeping previous values. ");
        System.out.print("Provide initial tab size (n>0): ");
        int size = input.nextInt();
        int x = size, y = size; //initial tab dimensions
        int xi = 0, yi = 0; //dimensions of previously generated tab
        int bl = x-1; //variable used to break line while printing

        int[][] tempTable = new int[x][y];
        System.out.println("\n"+"Initial tab: ");

        do {
            int[][] table = new int[x][y];
            if (x == (size*2)) System.out.println("Larger tab: ");
                else if (x == (size*3)) System.out.println("Largest tab: ");
            for (int i=0; i<table.length; i++)
                for (int j=0; j < table[i].length; j++) {
                    if ((i >= xi) || (j >= yi)) table[i][j]=digit.nextInt(9)+1; //random digit from 1 to 9
                        else table[i][j]=tempTable[i][j]; //value from previous tab
                    if (j == bl) System.out.print(table[i][j]+"\n");
                        else System.out.print(table[i][j]+" ");
                }

            if (x > size) tempTable = new int[x][y]; //temporary tab to store values from previous tab
            if ((x < (size*3)) && (y < (size*3))) {
                for (int i2 = 0; i2 < tempTable.length; i2++)
                    for (int j2 = 0; j2 < tempTable[i2].length; j2++) {
                        tempTable[i2][j2] = table[i2][j2];
                    }
            }
            System.out.println();

            //incrementing variables
            xi = x;
            yi = y;
            x = x + size;
            y = y + size;
            bl = bl + size;

        } while ((x <= (size*3)) && (y <= (size*3)));

    }
}
