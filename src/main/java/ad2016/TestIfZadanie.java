package ad2016; /**
 * Created by kokos on 2016-05-31.
 * Javastart.pl
 * 1.9 Pobierz w konsoli dwie liczby całkowite, następnie porównaj je i wyświetl stosowny komunikat z wynikiem.
 */
import java.util.Scanner;

public class TestIfZadanie {
    public static void main(String[] args){

        int x;
        Scanner odczyt = new Scanner(System.in);
        System.out.print("Podaj 1. liczbę: ");
        x = odczyt.nextInt();
        System.out.print("Podaj 2. liczbę: ");
        int y = (new Scanner(System.in).nextInt()); //można też w ten sposób zainicjalizować i zadeklarować zmienna y

        if (x==y)
            System.out.println("Liczby są równe");
        else if (x>y)
            System.out.println("Liczba 1. jest większa niż 2.");
        else
            System.out.println("Liczba 2. jest większa niż 1.");

    }
}
