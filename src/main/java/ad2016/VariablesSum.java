package ad2016;

/**
 * Created by konradal on 14/09/2016. Javastart.pl
 */
public class VariablesSum {
    public int sum (int arg0, int...args) {
        int outcome = arg0;
        for (int i=0; i<args.length; i++) {
            outcome += args[i];
        }
        return outcome;
    }
}
