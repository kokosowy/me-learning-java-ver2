package ad2016;

/**
 * Created by kokos on 2016-08-17. Javastart.pl
 */

public class Firma {
	public static void main (String[] args) {
		FirmaPracownik prac = new FirmaPracownik ("Włodek", "Zięba", 3000);
		System.out.println("Imię: "+prac.imie);
		System.out.println("Nazwisko: "+prac.nazwisko);
		System.out.println("Wypłata: "+prac.wyplata+"\n");
		
		//najpierw stwórzmy domyślny obiekt klasy ad2016.FirmaSzef korzystając z domyślnego konstruktora
		//odziedziczonego z klasy ad2016.FirmaPracownik
		FirmaSzef szef = new FirmaSzef();
		
		//zobaczmy jak wyglądają odpowiednie pola
		System.out.println("Imię: "+szef.imie);
		System.out.println("Nazwisko: "+szef.nazwisko);
		System.out.println("Wypłata: "+szef.wyplata);
		System.out.println("Premia: "+szef.premia+"\n");
		
		//teraz ustawimy dane szefa
		szef.imie = "Tadeusz";
		szef.nazwisko = "Kowalski";
		szef.wyplata = 10000;
		szef.premia = 2000;
		System.out.println("Imię: "+szef.imie);
		System.out.println("Nazwisko: "+szef.nazwisko);
		System.out.println("Wypłata: "+szef.wyplata);
		System.out.println("Premia: "+szef.premia+"\n");
	}
}		
