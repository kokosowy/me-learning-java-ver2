package ad2016; /**
 * Created by kokos on 2016-09-20. Javastart.pl
 * Nasz komparator porównuje dwa obiekty typu pracownik za pomocą metody compare(). Porównanie odbywa się
 * na podstawie wysokości wypłat w naturalnym rosnącym porządku. Warto zauważyć, że jest to klasa generyczna, w naszym
 * przypadku wyspecjalizowana do porównywania obiektów typu Pracownik.
 */
import java.util.Comparator;

public class ArraysEmployeeCompare implements Comparator<ArraysEmployee> {
    //@Override - according to javastart.pl
    public int compare(ArraysEmployee e1, ArraysEmployee e2) {
        if (e2 == null) return -1;
        if (e1.getSalary() > e2.getSalary()) return 1;
        else if (e1.getSalary() < e2.getSalary()) return -1;
        else return 0;
    }
}
