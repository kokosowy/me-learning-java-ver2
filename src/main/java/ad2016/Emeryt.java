package ad2016;

/**
 * Created by kokos on 2016-09-06. Javastart.pl
 */
public abstract class Emeryt {
    public static final int ILOSC_OCZU = 2; //stałe są OK

    //metoda abstrakcyjna
    public abstract String krzyczyNaDzieci();

    //zwykła metoda z implementacją
    public static void biegnijDoSklepu (int odleglosc, int predkosc) {
        double czas = (double)odleglosc/predkosc;
        System.out.println("Biegnę po kiełbasę. Będę za "+czas);
    }
}
/*
NOTE: a tak wygląda błędna implementacja klasy abstrakcyjnej
public abstract class ad2016.Emeryt {
    public static final int ILOSC_OCZU = 2; //stałe są ok

    //metoda abstrakcyjna nie może być statyczna
    public abstract static String krzyczNaDzieci();

    //metoda abstrakcyjna nie może posiadać implementacji
    //ani nawet nawiasów klamrowych
    public abstract void biegnijDoSklepu() {}
}*/
