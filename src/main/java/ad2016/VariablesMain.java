package ad2016;

/**
 * Created by konradal on 14/09/2016. Javastart.pl
 */
public class VariablesMain {
    public static void main (String[] args) {

        VariablesSum vsum = new VariablesSum();
        System.out.println("sum: 3 = "+vsum.sum(3)+"\n");
        System.out.println("sum: 3+3 = "+vsum.sum(3,3)+"\n");
        System.out.println("sum: 3+3+3 = "+vsum.sum(3,3,3)+"\n");

        VariablesPrint vprint = new VariablesPrint();
        vprint.print(3);
        vprint.print(3,3);
        vprint.print(3,3,3);

    }
}
