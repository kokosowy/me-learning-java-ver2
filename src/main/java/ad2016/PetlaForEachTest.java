package ad2016;

/**
 * Created by kokos on 2016-07-05. Javastart.pl
 */
public class PetlaForEachTest {
    public static void main (String[] args) {
        int[] table = new int[10];
        //wypełnianie tablicy pętlą 'for'
        for (int i=0; i<10; i++)
            table[i]=i;

        //drukowanie zawartości tablicy pętlą 'for each'
        for (int x : table)
            System.out.println(table[x]);

    }
}
