package ad2016; /**
 * Created by kokos on 2016-08-31. Javastart.pl
 */
 import java.io.BufferedReader;
 import java.io.IOException;
 import java.io.InputStreamReader;
 
 public class ReadExceptionsBest {
	public static void main (String[] args) {
		int tab[] = {1, 2, 3, 4, 5};
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		int index = -1;
		
		System.out.print("What tab element you would like to see (0-4)? ");
		boolean isCorrect = false;
		
		while (!isCorrect) {
			try {
				index = Integer.parseInt(input.readLine());
			} catch (NumberFormatException n) {
				System.out.print("Incorrect value."+
				"\nWhat tab element you would like to see (0-4)? ");
			} catch (IOException e) {
				System.out.println("Data input error.");
			}
			isCorrect = index == -1? false : true; //tricky
		}
		
		try {
			 System.out.println("tab["+index+"] = "+tab[index]);
		} catch (ArrayIndexOutOfBoundsException e) {
			 System.out.println("Incorrect value. Table's length is "+tab.length);
		}
			
	}
 }
