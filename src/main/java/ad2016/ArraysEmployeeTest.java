package ad2016; /**
 * Created by kokos on 2016-09-20. Javastart.pl
 * Na koniec klasa testowa. Najpierw utworzyliśmy kilku pracowników i dodaliśmy ich do tablicy tego typu. Następnie
 * musieliśmy utworzyć także odpowiedni komparator do porównywania elementów. Za pomocą statycznej metody sort() z klasy
 * Arrays posortowaliśmy wcześniej przygotowaną tablicę w rosnącym porządku według wysokości wypłat.
 * Warto zauważyć, że tablice możemy traktować jak struktury implementujące interfejs Iterable i operować na nich
 * przy pomocy foreach - u nas w celu wyświetlenia kolejnych elementów.
 */
import java.util.Arrays;

public class ArraysEmployeeTest {
    public static void main (String[] args) {
        //create few employee
        ArraysEmployee krzysiek = new ArraysEmployee("Krzysiek", "Piotrowicz", 2000);
        ArraysEmployee piotrek = new ArraysEmployee("Piotrek", "Wolny", 3000);
        ArraysEmployee kasia = new ArraysEmployee("Kasia", "Krotwicka", 2500);
        ArraysEmployee wlodek = new ArraysEmployee("Włodek", "Zięba", 2300);

        //put employees in table
        ArraysEmployee[] empl = {krzysiek, piotrek, kasia, wlodek};
        show(empl);
        //create comparator object
        ArraysEmployeeCompare comp = new ArraysEmployeeCompare();
        //sort table using comparator
        System.out.println("\n"+"Sorting...");
        Arrays.sort(empl, comp);
        //print sorted table
        show(empl);
    }

    public static void show (ArraysEmployee[] empl) {
        for (ArraysEmployee em : empl)
            System.out.println(em);
    }
}
