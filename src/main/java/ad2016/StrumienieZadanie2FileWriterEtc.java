package ad2016; /**
 * Created by konradal on 27/07/2016.
 * ad2016.StrumienieZadanie2 z użyciem BufferedWriter/Reader oraz FileWriter/Reader
 */
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;

public class StrumienieZadanie2FileWriterEtc {

    public static void main(String[] args) {
        String pathCopy = "pliki/binarnie2.txt"; //location of file to be copied
        String pathPaste = "pliki/przekopiowane2.txt"; //location of file to be pasted
        BufferedReader toCopy = null;
        BufferedWriter toPaste = null;
        String line = "";

        try {
            toCopy = new BufferedReader(new FileReader(pathCopy));
            toPaste = new BufferedWriter(new FileWriter(pathPaste));
        } catch (IOException e) {
            System.out.println("Some files are missing."); //handling 'file not found' exception
        }

        //int bytesToCopy = 0, bytesPasted = 0;
        //byte[] buffer = new byte[1024]; //creating byte table - buffer for data

        try {
            while ((line = toCopy.readLine()) != null) { //reading data, storing it in buffer and checking if all data is read
                toPaste.write(line); //writing to file from buffer, from position 0, all the bytes stored
                toPaste.newLine();
                //		bytesPasted += bytesToCopy;
            }
        } catch (IOException e) {
            System.out.println("Input-output error.");
        }

        try {
            if (toCopy != null) toCopy.close();
            if (toPaste != null) toPaste.close();
        } catch (IOException e) {
            System.out.println("Error while closing streams.");
        }

        //System.out.println("Amount of bytes copied: "+bytesPasted);

    }

}
