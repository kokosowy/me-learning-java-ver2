package ad2016;

/**
 * Created by kokos on 2016-08-24. Javastart.pl
 */
public class EmployeeMain {
    public static void main (String[] args) {
        Employee employee = new Employee("Marian", "Dudek", 2500);
        EmployeeNurse nurse = new EmployeeNurse("Asia", "Kowalska", 4000);
        nurse.setOvertime(18);
        EmployeeDoctor doctor = new EmployeeDoctor("Zygmunt", "Nowak", 7000);
        doctor.setBonus(3000);

        System.out.print("New employees: "+"\n");
        System.out.print("1) "+employee.getName()+" "+employee.getSurname()+", salary: "+employee.getSalary()+"\n");
        System.out.print("\t"+"\t"+"no overtime/bonus"+"\n");
        System.out.print("2) "+nurse.getName()+" "+nurse.getSurname()+", salary: "+nurse.getSalary()+"\n");
        System.out.print("\t"+"\t"+"overtime: "+nurse.getOvertime()+"\n");
        System.out.print("3) "+doctor.getName()+" "+doctor.getSurname()+", salary: "+doctor.getSalary()+"\n");
        System.out.print("\t"+"\t"+"bonus: "+doctor.getBonus()+"\n");
    }
}
