package ad2016;

/**
 * Created by konradal on 26/07/2016. Javastart.pl
 * 2.4 Napisz program składający się z dwóch klas. Pierwsza niech zawiera kilka metod o nazwie dodaj(), ale zwracających
 * różne typy wyników i przyjmujących po minimum dwa argumenty typów liczbowych wybranych przez Ciebie. Ich zadaniem
 * jest zwrócenie, lub wyświetlanie sumy podanych argumentów. W drugiej klasie Testowej utwórz obiekt tej klasy i sprawdź
 * działanie swoich metod, wyświetlając wyniki działań na ekranie. Dodatkowo każda z metod niech wyświetla
 * swój typ zwracany i rodzaj argumentów, abyś wiedział, która z nich zadziałała.
 */
public class Overloading {

    int add(byte a, byte b) {
        System.out.println("int byte (byte a, byte b): ");
        return a + b;
    }

    int add(int a, int b) {
        System.out.println("int add (int a, int b): ");
        return a + b;
    }

    char add(char a, char b) {
        System.out.println("char add (char a, char b): ");
        return (char) (a + b);
    }

    double add(double a, double b) {
        System.out.println("double add (double a, double b): ");
        return (double) (a + b);
    }

    float add(float a, float b) {
        System.out.println("float add (float a, float b): ");
        return a + b;
    }

    long add(long a, long b) {
        System.out.println("long add (long a, long b): ");
        return (long) (a + b);
    }

    short add(short a, short b) {
        System.out.println("short add (short a, short b): ");
        return (short) (a + b);
    }

    String add(String a, String b) {
        System.out.println("String add (String a, String b): ");
        return (a + b);
    }

}
