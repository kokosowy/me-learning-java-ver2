package ad2016;

/**
 * Created by kokos on 2016-09-20. Javastart.pl
 * Klasa ad2016.ArraysEmployee przechowuje podstawowe informacje: imię i nazwisko, a także wysokość wypłaty. Dodatkowo
 * przesłonięto metodę toString() klasy Object w celu wygodnego wyświetlania danych w późniejszej części.
 */
public class ArraysEmployee {
    private String name;
    private String surname;
    private double salary;

    public ArraysEmployee(String name, String surname, double salary) {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    String getName() { return name; }
    String getSurname() { return surname; }
    double getSalary() { return salary; }

    public String toString() {
        return "Name: "+name+", Surname: "+surname+", Salary: "+salary;
    }
}
