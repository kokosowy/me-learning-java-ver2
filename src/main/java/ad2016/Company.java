package ad2016;

/**
 * Created by konradal on 19/07/2016. Javastart.pl
 * 2.1 Napisz klasę Pracownik, która przechowuje trzy pola:
 -Imię
 -Nazwisko
 -Wiek
 Następnie utwórz klasę ad2016.Firma, w której wykorzystasz klasę pracownik do utworzenia dwóch obiektów przechowujących
 dane pracowników (wymyśl sobie jakieś) i później wyświetlasz je na ekran.
 */
public class Company {
    public static void main (String[] args) {
        CompanyEmployee em1 = new CompanyEmployee();
        CompanyEmployee em2 = new CompanyEmployee();

        em1.setEmployee("James", "Bond", 40);
        em2.setEmployee("Tupac", "Shakur", 27);

        System.out.println("ad2016.Company's employees are: ");
        System.out.println(em1.getEmployee());
        System.out.println(em2.getEmployee());

    }
}
