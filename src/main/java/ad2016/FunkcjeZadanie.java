package ad2016; /**
 * Created by konradal on 24/05/2016.
 * Javastart.pl
 * 1.4 Napisz prosty kalkulator, w którym zainicjujesz dwie zmienne int a i b dowolnymi liczbami mieszczącymi się
 * w zakresie - niech a będzie liczbą ujemną, natomiast b dodatnią... Następnie wykorzystując import statyczny
 * wyświetl wyniki następujących działań bez użycia dodatkowych zmiennych:
 -a^b (a do potęgi b)
 -|a| (wartość bezwzględna z liczby a)
 -pierwiastek z liczby a podniesionej do potęgi b.
 Zobacz co się stanie, gdy do a i b przypiszesz dwie 3 cyfrowe liczby, oraz gdy liczba b będzie nieparzysta (wynik a^b będzie ujemny).

 1.5 Napisz program analogiczny do 1.4 wykorzystując klasę BigInteger. Utwórz dwie liczby wykraczające
 poza zakres long, wypróbuj funkcje dodawania, odejmowania i mnożenia, a także podniesienie do potęgi
 (zobacz w API jaki typ parametru przyjmuje ta funkcja) i wartość bezwzględną.
 Wszystkie wyniki zaprezentuj w konsoli.
 */

import java.math.BigInteger;

import static java.lang.Math.*;

public class FunkcjeZadanie {
    public static void main(String[] args){

        int a = -25;
        int b = 4;
        System.out.println(a+"^"+b+" = "+pow(a, b));
        System.out.println("|"+a+"| = "+abs(a));
        System.out.println("sqrt("+a+"^"+b+") = "+sqrt(pow(a, b)));
        System.out.println();

        BigInteger x = new BigInteger("123456789123456789123456789");
        BigInteger y = new BigInteger("987654321987654321987654321");
        BigInteger suma = x.add(y);
        System.out.println("Suma x+y = "+suma.toString());
        BigInteger roznica = x.subtract(y);
        System.out.println("Roznica x-y = "+roznica.toString());
        System.out.println("Iloczyn x*y = "+x.multiply(y).toString()); //mozna tez bez uzycia dodatkowych zmiennych
        System.out.println("Iloraz x/y = "+x.divide(y).toString());
        System.out.println("Potega x^y = "+x.pow(4).toString());
        System.out.println("Wartosc bezwzgledna |x| = "+x.abs().toString());

    }
}
