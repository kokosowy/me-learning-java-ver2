package ad2016; /**
 * Created by kokos on 2016-08-09. Javastart.pl
 * 2.5 Napisz program, który będzie się składał z dwóch klas:
 * - Pracownik - przechowująca takie dane jak imię, nazwisko i wiek pracownika, oraz co najmniej trzy
 * konstruktory, które posłużą do inicjowania obiektów z różnymi parametrami - w przypadku gdy przykładowo
 * konstruktor przyjmuje tylko 1 parametr, zainicjuj pozostałe pola jakimiś domyślnymi wartościami.
 * - ad2016.Firma - klasa testowa, w której utworzysz kilka obiektów klasy Pracownik i wyświetlisz dane na ekran.
 * Mile widziane wykorzystanie tablic oraz pętli.
 */
import java.util.Scanner;

public class CompanyConstructMain {
    public static void main (String[] args) {
        CompanyConstruct[] employee = new CompanyConstruct[6];
        Scanner inputString = new Scanner(System.in);
        Scanner inputInt = new Scanner(System.in);
        String inputName;
        String inputSurname;
        int inputAge;
        int id = 0;

        //employee created from keyboard
        System.out.print("Provide employee's name: ");
        inputName = inputString.nextLine();
        System.out.print("Provide employee's surname: ");
        inputSurname = inputString.nextLine();
        System.out.print("Provide employee's age: ");
        inputAge = inputInt.nextInt();
        employee[id] = new CompanyConstruct(inputName, inputSurname, inputAge);
        id++;

        //employee created from tables
        for (int i = 0; (i < 3); i++) {
            employee[id] = new CompanyConstruct(i);
            id++;
        }

        //employee created from employee
        employee[id] = new CompanyConstruct(employee[0]);
        id++;

        //employee created old fashion way
        employee[id] = new CompanyConstruct();
        employee[id].name = "Konrad";
        employee[id].surname = "Alfawicki";
        employee[id].age = 31;

        //printing all employees
        System.out.println("\n"+"Register of employees:");
        for (CompanyConstruct e : employee)
            System.out.println("> "+e.name+" "+e.surname+", age "+e.age);


    }
}
