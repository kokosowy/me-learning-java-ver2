package ad2016;

/**
 * Created by konradal on 22/07/2016. Javastart.pl
 * http://javastart.pl/static/programowanie-obiektowe/argumenty-metod/
 */
public class PointSetChangeMain {
    public static void main (String[] args) {
        PointSet point = new PointSet();
        point.crdtX = 5; //can be point.setX(5);
        point.crdtY = 5; //can be point.setY(5);

        System.out.println("Coordinates are: ("+point.crdtX+", "+point.crdtY+")"); //can be point.retX() etc.

        System.out.println("Changing...");
        PointSetChange.change(point);

        System.out.println("Coordinates are: ("+point.crdtX+", "+point.crdtY+")");
    }
}
