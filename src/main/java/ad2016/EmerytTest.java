package ad2016;

/**
 * Created by kokos on 2016-09-06. Javastart.pl - klasa testująca na podstawie komentarza.
 */
public class EmerytTest {
    public static void main (String[] args) {
        Emeryt dziadek = new Emeryt() {
            String str = "Tak dziadek krzyczy";

            public String krzyczyNaDzieci() {
                System.out.println(str);
                return str;
            }
        };

        dziadek.krzyczyNaDzieci();

        System.out.println("Ilość oczu u dziadka: "+Emeryt.ILOSC_OCZU);

        Emeryt.biegnijDoSklepu(10, 4);
    }
}
