package ad2016;

/**
 * Created by kokos on 2016-08-09. Javastart.pl
 * 2.5 Napisz program, który będzie się składał z dwóch klas:
 * - Pracownik - przechowująca takie dane jak imię, nazwisko i wiek pracownika, oraz co najmniej trzy
 * konstruktory, które posłużą do inicjowania obiektów z różnymi parametrami - w przypadku gdy przykładowo
 * konstruktor przyjmuje tylko 1 parametr, zainicjuj pozostałe pola jakimiś domyślnymi wartościami.
 * - ad2016.Firma - klasa testowa, w której utworzysz kilka obiektów klasy Pracownik i wyświetlisz dane na ekran.
 * Mile widziane wykorzystanie tablic oraz pętli.
 */
public class CompanyConstruct {
    String name;
    String surname;
    int age;
    String[] names = {"Adam", "John", "Aleksandra"};
    String[] surnames = {"Jensen", "Locke", "Romanoff"};
    int[] ages = {33, 44, 22};

    CompanyConstruct () {};

    CompanyConstruct (int i) {
        name = names[i];
        surname = surnames[i];
        age = ages[i];
    }

    CompanyConstruct (String empName, String empSurname, int empAge) {
        name = empName;
        surname = empSurname;
        age = empAge;
    }

    CompanyConstruct (CompanyConstruct emp) {
        name = emp.name;
        surname = emp.surname;
        age = emp.age;
    }
}
