package ad2016; /**
 * Created by kokos on 2016-06-22.
 * Javastart.pl
 * 1.8 Napisz program, w którym wprowadzisz w konsoli swoje imię, następnie zapiszesz je do pliku.
 * Odczytaj je z powrotem z pliku i bez użycia dodatkowej zmiennej wyświetl na ekranie.
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class PlikZadanie {
    public static void main(String[] args) throws FileNotFoundException {

        //pobranie imienia do zmiennej
        Scanner input = new Scanner(System.in);
        System.out.print("Podaj imię: ");
        String name = input.nextLine();

        //zapisanie zmiennej do pliku
        PrintWriter file = new PrintWriter("pliki/ala.txt");
        file.print(name);
        file.close();

        //odczyt pliku
        Scanner output = new Scanner(new File("pliki/ala.txt"));
        /*
        * poprzednią linię można zastąpić dwiema:
        * File readfile = new File("wiedza/ala.txt");
        * Scanner output = new Scanner(readfile);
        */
        System.out.println("Twoje imię to: ");
        System.out.println(output.nextLine());

    }
}
