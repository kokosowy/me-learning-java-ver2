package ad2016;

/**
 * Created by konradal on 19/07/2016. Javastart.pl
 * 2.1 Napisz klasę Pracownik, która przechowuje trzy pola:
 -Imię
 -Nazwisko
 -Wiek
 Następnie utwórz klasę ad2016.Firma, w której wykorzystasz klasę pracownik do utworzenia dwóch obiektów przechowujących
 dane pracowników (wymyśl sobie jakieś) i później wyświetlasz je na ekran.
 */
public class CompanyEmployee {
    String name;
    String surname;
    int age;

    void setEmployee (String empName, String empSurname, int empAge) {
        name = empName;
        surname = empSurname;
        age = empAge;
    }

    String getEmployee() {
        return (surname+", "+name+" ("+age+")");
    }

}
