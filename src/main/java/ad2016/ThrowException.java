package ad2016; /**
 * Created by kokos on 2016-09-04. Javastart.pl
 */
import java.util.Scanner;

public class ThrowException {
    public static void main (String[] args) throws ArithmeticException {
        int x = 10;
        int y;
        Scanner input = new Scanner (System.in);
        System.out.print("Provide divisor: ");
        y = input.nextInt();
        if (y==0)
            throw new ArithmeticException("You can't divide by 0");
        else
            System.out.println("10 / "+y+" = "+x/(double)y);
    }
}
