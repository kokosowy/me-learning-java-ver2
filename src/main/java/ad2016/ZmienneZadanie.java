package ad2016;

/**
 * Created by konradal on 10/05/2016.
 * Javastart.pl
 * 1.2. Napisz program, w którym zadeklarujesz kilka stałych, lub zmiennych różnych typów o dowolnych
 * nazwach, a następnie wyświetlisz je w kolejnych liniach tekstu. Skompiluj, lub spróbuj skompilować
 * przykłady podane w tej lekcji i zobacz co się stanie przy próbie nadania po raz drugi wartości jakiejś stałej.
 *
 * W tym samym programie zadeklaruj cztery zmienne typu String. Trzy z nich zainicjuj jakimiś wyrazami a czwartemu
 * przypisz ciąg znaków utworzony z trzech wcześniejszych zmiennych. Następnie wyświetl czwartą zmienną na ekranie.
 * Przy użyciu metody substring wyświetl na ekranie dwa pierwsze wyrazy wykorzystując wyłącznie czwartą zmienną typu String.
 */
public class ZmienneZadanie {
    public static void main(String[] args){

        int i = 12345;
        double j = 1.2345;
        char znak = '2';
        byte bajt = 11;
        final int SUMA;
        SUMA = 10000 + 2345;
        System.out.println("Zmienna int: "+i);
        System.out.println("Zmienna double: "+j);
        System.out.println("Zmienna char: "+znak);
        System.out.println("Zmienna byte: "+bajt);
        System.out.println("Stala int: "+SUMA);

        String t1 = "raz, ";
        String t2 = "dwa, ";
        String t3 = "trzy!";
        String t4 = t1+t2+t3;
        System.out.println(t4);
        String konkluzja = t4.substring(0,(t1.length()+t2.length()));
        System.out.println(konkluzja+"\n"+"\t"+t4.substring((t1.length()+t2.length()), t4.length()));

    }
}
