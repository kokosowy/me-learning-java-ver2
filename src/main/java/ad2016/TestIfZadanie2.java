package ad2016; /**
 * Created by kokos on 2016-05-31.
 * Javastart.pl
 * 1.10 Napisz program, który pobierze od użytkownika imię i przechowa je w zmiennej. Następnie stwórz
 * kilka warunków z różnymi imionami. Jeśli któreś z imion będzie pasowało wyświetl "Cześć jakieś_imię", gdy program
 * nie znajdzie imienia wyświetl "Przykro mi, ale Cię nie znam".
 */
import java.util.Scanner;

public class TestIfZadanie2 {
    public static void main(String[] args){

        System.out.print("Jak masz na imię? ");
        Scanner odczyt = new Scanner(System.in);
        String imie = odczyt.nextLine();

        if (imie.equals("Adam")) //niestety nie dało się zrobić tego switchem, który służy tylko do int, char etc.
            System.out.println("Cześć " + imie);
        else if ("Asia".equals(imie)) //tak też można zapisać
            System.out.println("Hej " + imie);
        else if (imie.equals("Konrad"))
            System.out.println("Siema " + imie);
        else
            System.out.println("Przykro mi, ale Cię nie znam");

    }
}
