package ad2016;

/**
 * Created by konradal on 14/09/2016. Javastart.pl
 */
public class VariablesPrint {
    public void print(int arg0, int...args) {
        System.out.println("1. argument stały: "+arg0);
        for (int i=0; i<args.length; i++) {
            System.out.println((i+1)+". argument zmienny: "+args[i]);
        }
        System.out.println();
    }
}
