package ad2016;

/**
 * Created by konradal on 19/07/2016. Javastart.pl
 * 2.2 Zmodyfikuj powyższy program tak, aby utworzyć trzech pracowników, a odpowiednie pola zainicjuj
 * wartościami z wcześniej utworzonych tablic (dowolne dane) przy użyciu pętli.
 */
public class Company2 {
    public static void main (String[] args) {
        CompanyEmployee[] em = new CompanyEmployee[3];

        String[] names = {"James", "Tupac", "Anna"};
        String[] surnames = {"Bond", "Shakur", "Karenina"};
        int[] ages = {40, 27, 31};

        for (int i = 0; i < em.length; i++) {
            em[i] = new CompanyEmployee();
            em[i].setEmployee(names[i], surnames[i], ages[i]);
        }

        System.out.println("ad2016.Company's employees are: ");

        for (int i = 0; i < em.length; i++) {
            System.out.println(em[i].getEmployee());
        }

    }
}
