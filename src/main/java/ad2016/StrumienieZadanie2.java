package ad2016; /**
 * Created by konradal on 08/07/2016. Javastart.pl
 * 1.16 Korzystając z własnego buforowania przekopiuj dane znajdujące się w pliku binarnie.txt do przekopiowane.txt.
 * Dodatkowo zlicz ilość skopiowanych bajtów.
 * (Podpowiedź: użyj metody write(byte[] b, int off, int len), dostępnej w każdej z dzisiaj omówionych klas).
 */
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;

public class StrumienieZadanie2 {

    public static void main (String[] args) {
		String pathCopy = "pliki/binarnie.txt"; //location of file to be copied
		String pathPaste = "pliki/przekopiowane.txt"; //location of file to be pasted
		DataInputStream toCopy = null;
		DataOutputStream toPaste = null;

		try {
			toCopy = new DataInputStream(new FileInputStream(pathCopy));
			toPaste = new DataOutputStream(new FileOutputStream(pathPaste));
		} catch (FileNotFoundException e) {
			System.out.println("Some files are missing."); //handling 'file not found' exception
		}

		int bytesToCopy = 0, bytesPasted = 0;
		byte[] buffer = new byte[1024]; //creating byte table - buffer for data

		try {
			while ((bytesToCopy = toCopy.read(buffer)) != -1) { //reading data, storing it in buffer and checking if all data is read
				toPaste.write(buffer, 0, bytesToCopy); //writing to file from buffer, from position 0, all the bytes stored
				bytesPasted += bytesToCopy;
			}
		} catch (IOException e) {
			System.out.println("Input-output error.");
		}
		
		try {
			if (toCopy != null) toCopy.close();
			if (toPaste != null) toPaste.close();
		} catch (IOException e) {
			System.out.println("Error while closing streams.");
		}
		
		System.out.println("Amount of bytes copied: "+bytesPasted);

    }
}
