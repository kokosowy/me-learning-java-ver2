package ad2016;

/**
 * Created by kokos on 2016-09-05. Javastart.pl
 */
public class PolymorphismObject {
    public static void main (String[] args) {
        String str1 = new String("Siabada1");
        Object str2 = new String("Siabada2");
        System.out.println(str1);
        System.out.println(str2);
    }
}
