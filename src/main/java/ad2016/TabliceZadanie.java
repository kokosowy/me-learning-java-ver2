package ad2016; /**
 * Created by kokos on 2016-06-27. Javastart.pl
 * 1.12 Napisz program, w którym zadeklarujesz i utworzysz pięcioelementową tablicę odpowiedniego typu. W pętli
 * pobierzesz od użytkownika 5 imion i je w niej zapiszesz. Następnie wyświetl na ekranie powiadomienia
 * "Witaj imie_z_tablicy" dla każdego z podanych parametrów.
 */
import java.util.Scanner;

public class TabliceZadanie {
    public static void main (String[] args) {
        Scanner name = new Scanner(System.in);
        String[] table = new String[5];

        for (int i = 0; i < 5; i++) {
            System.out.print("Podaj "+(i+1)+". imię: ");
            table[i] = name.nextLine();
        }

        for (int i = 0; i < 5; i++) System.out.println("Witaj "+table[i]);

    }
}
