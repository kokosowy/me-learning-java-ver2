package ad2016; /**
 * Created by kokos on 2016-08-30. Javastart.pl
 */
 import java.util.Scanner;
 
 public class ReadExceptions {
	 public static void main (String[] args) {
		 int tab[] = {1, 2, 3, 4, 5};
		 Scanner input = new Scanner (System.in);
		 int index = -1;
		 
		 System.out.print("Write index of the tab element you would like to see: ");
		 index = input.nextInt();
		 
		 System.out.println("tab["+index+"] = "+tab[index]);
	}
} 
