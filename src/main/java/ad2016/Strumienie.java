package ad2016; /**
 * Created by konradal on 08/07/2016. Javastart.pl
 */
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Strumienie {
    public static void main (String[] args) {
        //1st method - less elegant: everything in one block
        try {
            DataOutputStream stream1 = new DataOutputStream(new FileOutputStream("pliki/plik.txt"));
            /*
            * dowolne
            * metody
            *
            */
            stream1.close();
        } catch (FileNotFoundException e) {
            System.out.println("Sorry, file not found");
        } catch (IOException e) {
            System.out.println("Input-output error");
        }

        //2nd method - much more elegant and desired - initialization, using and closing stream is separated
        DataOutputStream stream2 = null;

        try {
            stream2 = new DataOutputStream(new FileOutputStream("pliki/plik.txt"));
        } catch (FileNotFoundException e) {
            System.out.println("Sincere apologies Sir, file not found");
        }

        try {
            /*
            * dowolne metody
            */
            stream2 = new DataOutputStream(new FileOutputStream("pliki/plik.txt")); //added swiftly so class will compile
        } catch (IOException e) {
            e.printStackTrace(); // exception handling
        }

        try {
            if (stream2 != null)
                stream2.close();
        } catch (IOException e) {
            System.out.println("Apologies Sir - closing stream error occurred");
        }
    }
}
