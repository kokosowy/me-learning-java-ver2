package ad2016;

/**
 * Created by kokos on 2016-06-29. Javastart.pl
 * 1.14 Przy użyciu pętli i tablic przechowujących liczby całkowite zaprezentuj poniższą treść:
 tab[0,0] = 0;
 tab[0,1] = 1;
 tab[0,2] = 2;
 tab[1,0] = 3;
 tab[1,1] = 4;
 tab[1,2] = 5;
 Wykorzystuj przy tym własność length.
 */
public class TabliceWielowymZadanie {
    public static void main (String[] args) {
        int[][] table = new int[2][3];
        int n=0;

        for (int i=0; i<table.length; i++) {
            for (int j=0; j<table[i].length; j++) {
                table[i][j]=n; //tab[i][j] = n++; //zamiast dwóch następnych linii - Javastart.pl
                n++;
            }
        }

        for (int i=0; i<table.length; i++) {
            for (int j = 0; j < table[i].length; j++)
                System.out.println("tab["+i+","+j+"] = "+table[i][j]);
                //System.out.printf("tab[%d][%d]=%d \n", i, j, tab[i][j]); //tak też można - Javastart.pl
        }

        System.out.println();

        for (int i=0; i<table.length; i++) {
            for (int j = 0; j < table[i].length; j++)
                System.out.print(table[i][j]);
            System.out.println();
        }

    }
}
