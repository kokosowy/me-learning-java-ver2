package ad2016;

/**
 * Created by konradal on 19/07/2016. Javastart.pl
 */
public class PointSet {
    int crdtX;
    int crdtY;

    void setX (int x) {
        crdtX = x;
    }

    void setY (int y) {
        crdtY = y;
    }

    int retX() {
        return crdtX;
    }

    int retY() {
        return crdtY;
    }
}
