package ad2016; /**
 * Created by kokos on 2016-09-19. Javastart.pl
 */
import java.util.List;
import java.util.ArrayList;

public class ArrayListTest {
    public static void main (String[] args) {
        //create list
        List<String> list = new ArrayList<String>();
        //add Object type elements, that is of any type - in this case Strings
        list.add("Asia");
        list.add("Bartek");
        list.add("Krzysiek");
        list.add("Wojtek");

        //print list elements
        System.out.println("List elements: ");
        for (String elem : list)
            System.out.println(elem);
        System.out.println();

        //check list size
        System.out.println("List size is "+list.size());

        //delete "Asia" element and check size
        list.remove("Asia");
        System.out.println("After deleting 'Asia', list size is "+list.size());

        //delete element on second place, that is with index 1 - here "Krzysiek"
        list.remove(1);
        System.out.println("After deleting element with index 1, list size is "+list.size());

        //get first element from the list (need to be converted to String), set as variable and print it
        String name = (String)list.get(0);
        System.out.println("At the end first element of the list is "+name);
    }
}
