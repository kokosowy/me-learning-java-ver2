package ad2016;

/**
 * Created by kokos on 2016-10-09. Javastart.pl
 */
public class Strings3 {
	public static void main (String[] args) {
		String s = "a";
		long start = System.nanoTime();
		StringBuffer sBuff = new StringBuffer(s);
		
		for (int i=0; i<10000; i++) {
			sBuff.append("a");
		}
		
		s = sBuff.toString();
		System.out.println("Time3: "+(System.nanoTime()-start));
	}
}
