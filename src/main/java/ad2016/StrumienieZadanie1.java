package ad2016; /**
 * Created by konradal on 08/07/2016. Javastart.pl
 * 1.15 Do pliku daneBinarne.txt zapisz swoje imię, aktualny rok i wynik dzielenia 50/4. Następnie z tego samego pliku
 * odczytaj ile znajduje się w nim bajtów.
 */
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.io.IOException;
import java.util.Scanner;
 
public class StrumienieZadanie1 {
    public static void main (String[] args) {
		String path = "pliki/daneBinarne.txt";
		RandomAccessFile stream = null;
		int amountBytes = 0;
		
		System.out.print("What is your name? ");
		Scanner input = new Scanner(System.in);
		String name = input.nextLine();
		int year = 2016;
		double div = 50.0/4;
		
		try {
			stream = new RandomAccessFile(path, "rw"); 
		} catch (FileNotFoundException e) { //catches exception if file not found
			System.out.println("Sorry, file not found.");
		}
		
		try {
            stream = new RandomAccessFile(path, "rw");
            stream.writeUTF(name); //writes String to file and so on
            stream.writeInt(year);
            stream.writeDouble(div);
            
			stream = new RandomAccessFile(path, "rw"); //remember: you need to reset so called pointer, so it's possible to read from file
			while (stream.read() != -1) { //remember: this is how you count all bytes that have been read
				amountBytes++;
			}
        } catch (IOException e) { //catches other exceptions
            e.printStackTrace();
        }
        
        try {
			stream.close(); 
		} catch (IOException e) { //catches exception while closing stream
			e.printStackTrace();
		}
        
        System.out.println("Amount of bytes read is: "+amountBytes);
				
    }
}
