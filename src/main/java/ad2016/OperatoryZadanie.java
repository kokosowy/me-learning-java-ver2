package ad2016;

/**
 * Created by konradal on 18/05/2016.
 * Javastart.pl
 * 1.3 Napisz prosty kalkulator, w którym będziesz przechowywał 3 zmienne typu double o nazwach a,b,c.
 * Wypróbuj wszystkie operatory matematyczne:
 * (a+b)*c
 * a-b/c
 * użyj operatorów inkrementacji i zwiększ wszystkie zmienne o 1.
 * Teraz porównaj ze sobą, czy:
 * (a+b)>c
 * czy a=b?
 * Przedstaw wyniki w konsoli.
 */
public class OperatoryZadanie {
    public static void main(String[] args){

        double a = 3.5;
        double b = 5;
        double c = 1.25;
        double wynik;

        wynik = (a+b)/c;
        System.out.println("(a+b)/c = "+wynik);
        wynik = a-b/c;
        System.out.println("a-b/c = "+wynik);
        System.out.println();

        a++;
        b++;
        c++;

        System.out.println("Czy ("+a+" + "+b+") > "+c+" ? "+((a+b)>c));
        System.out.println("Czy "+a+" = "+b+" ? "+(a==b));

    }
}
