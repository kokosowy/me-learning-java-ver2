package ad2016;

/**
 * Created by kokos on 2016-10-09. Javastart.pl
 */
public class Strings2 {
	public static void main (String[] args) {
		String s = "a";
		long start = System.nanoTime();
		StringBuilder sB = new StringBuilder(s);
		
		for (int i=0; i<10000; i++) {
			sB.append("a");
		}
		
		s = sB.toString();
		System.out.println("Time2: "+(System.nanoTime()-start));
	}
}
