package ad2016;

/**
 * Created by konradal on 26/07/2016. Javastart.pl
 * 2.4 Napisz program składający się z dwóch klas. Pierwsza niech zawiera kilka metod o nazwie dodaj(), ale zwracających
 * różne typy wyników i przyjmujących po minimum dwa argumenty typów liczbowych wybranych przez Ciebie. Ich zadaniem
 * jest zwrócenie, lub wyświetlanie sumy podanych argumentów. W drugiej klasie Testowej utwórz obiekt tej klasy i sprawdź
 * działanie swoich metod, wyświetlając wyniki działań na ekranie. Dodatkowo każda z metod niech wyświetla
 * swój typ zwracany i rodzaj argumentów, abyś wiedział, która z nich zadziałała.
 */
public class OverloadingMain {
    public static void main (String[] args) {
        Overloading sum = new Overloading();

        System.out.println(sum.add((byte)(55), (byte)(55))); //if not forced it is considered int type

        System.out.println(sum.add(55, 55));

        System.out.println(sum.add((char)(55), (char)(55))); //if not forced it is considered int type

        System.out.println(sum.add((short)(55), (short)(55))); //if not forced it is considered int type

        System.out.println(sum.add(5.00005, 5.00005));

        System.out.println(sum.add((float)(5.00005), (float)(5.00005))); //if not forced it is considered double

        //System.out.println(sum.add((long)(5000000000005), (long)(5000000000005))); //it is still considered int type

        System.out.println(sum.add("5.00005", "5.00005"));

    }
}
