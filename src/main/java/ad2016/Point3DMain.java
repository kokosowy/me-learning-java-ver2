package ad2016;

/**
 * Created by konradal on 17/08/2016. Javastart.pl
 * 2.6 Stwórz klasę Punkt2D, która przechowuje informacje na temat punktu na przestrzeni dwuwymiarowej
 * (współrzędne x oraz y). Zawierająca dwa konstruktory: bezparametrowy ustawiający pola na wartość 0, oraz przyjmujący
 * dwa argumenty i ustawiający pola obiektu zgodnie z podanymi parametrami.
 * Napisz klasę Punkt3D dziedziczącą po Punkt2D, reprezentującą punkt w trójwymiarze (dodatkowe pole z).
 * W klasie testowej utwórz obiekty obu klas i przetestuj działanie.
 */
public class Point3DMain {
    public static void main(String[] args) {
        Point2D p2d = new Point2D(3,5);
        System.out.println("2D point's coordinates: ("+p2d.x+", "+p2d.y+")\n");

        Point3D p3d = new Point3D();
        p3d.x = 3;
        p3d.y = 4;
        p3d.z = 5;
        System.out.println("3D point's coordinates: ("+p3d.x+", "+p3d.y+", "+p3d.z+")\n");

        System.out.println("OR if we wish to inherit 2D point's coordinates as well...");

        p3d = new Point3D(p2d, 7);
        System.out.println("3D point's coordinates: ("+p3d.x+", "+p3d.y+", "+p3d.z+")\n");
    }
}