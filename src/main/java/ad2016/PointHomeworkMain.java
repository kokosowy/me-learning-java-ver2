package ad2016;

/**
 * Created by konradal on 25/07/2016. Javastart.pl
 * 2.3 Utwórz klasę Punkt, która przechowuje dwie wartości typu int - współrzędne punktu na powierzchni. Napisz w niej
 * także metody które:
 * - zwiększają wybraną współrzędną o 1
 * - zmieniają wybraną zmienną o dowolną wartość
 * - zwracają wartość współrzędnych (oddzielne metody)
 * - wyświetla wartość współrzędnych
 * Napisz także klasę, w której przetestujesz działanie metod wyświetlając działanie metod na ekranie,
 */
public class PointHomeworkMain {
    public static void main (String[] args) {
        PointHomework point = new PointHomework();
        point.crX = 3;
        point.crY = 3;

        System.out.print("Base coordinates: ");
        point.prntXY();

        point.incX();
        point.incY();
        System.out.print("Increased coordinates: ");
        point.prntXY();

        point.chngX(2);
        point.chngY(-2);
        System.out.print("Coordinates changed by (2,-2): ");
        point.prntXY();

        int x = point.retCrX();
        int y = point.retCrY();
        System.out.println("Coordinates are now: ("+x+", "+y+")");

        point.prntXY();
    }
}
