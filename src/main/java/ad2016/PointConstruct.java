package ad2016;

/**
 * Created by kokos on 2016-08-09. Javastart.pl
 */
public class PointConstruct {
    int x;
    int y;

    public PointConstruct () {}

    public PointConstruct (int a, int b) {
        x = a;
        y = b;
    }

    public PointConstruct (PointConstruct point) {
        x = point.x;
        y = point.y;
    }
}
