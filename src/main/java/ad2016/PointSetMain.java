package ad2016;

/**
 * Created by konradal on 19/07/2016. Javastart.pl
 */
public class PointSetMain {
    public static void main (String args[]) {
        PointSet point = new PointSet();
        point.setX(10); //if not using methods can be point.crdtX = 10;
        point.setY(20); //can be point.crdtY = 20;

        System.out.println("ad2016.PointSet's coordinates are: ("+point.retX()+","+point.retY()+")");
        //can be System.out.println("ad2016.PointSet's coordinates are: ("+point.crdtX+","+point.crdtY+")");
    }
}