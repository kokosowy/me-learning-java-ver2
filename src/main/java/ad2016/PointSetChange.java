package ad2016;

/**
 * Created by konradal on 22/07/2016. Javastart.pl
 * http://javastart.pl/static/programowanie-obiektowe/argumenty-metod/
 */
public class PointSetChange {
    static void change (PointSet pnt) {
        pnt.crdtX++; //changing external object's parameters - look into it if necessary
        pnt.crdtY++;
    }
}
