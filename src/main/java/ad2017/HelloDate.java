//: ad2017/HelloDate.java
package ad2017;

import java.util.*;

/** Pierwszy program przykladowy z 'Thinking in Java'.
 * Wyswietla ciag i dzisiejsza date.
 * @author Bruce Eckel
 * @author www.MindView.net
 * @version 4.0
 */
public class HelloDate {
    /** Punkt wejscia do klasy i aplikacji.
     * @param args tablica ciagow argumentow wywolania
     * @throws Exception nie zglasza wyjatkow
     */
    public static void main(String[] args) {
        System.out.println("Witaj, dzisiaj jest: ");
        System.out.println(new Date());
    }
} /* Output: (55% match)
Witaj, dzisiaj jest:
Mon Jul 10 00:03:45 CEST 2017
*///:~
