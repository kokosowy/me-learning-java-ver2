package ad2017;

import java.util.LinkedList;
import java.util.Random;

/**
 * Created by kokos on 2017-07-10.
 */
public class RandomExample {

    public static void nextRandomExample() {
        LinkedList m = new LinkedList();
        Random generator = new Random();

        for(int i=0; i<11; i++) {
            m.add(generator.nextDouble()*10);
        }

        for (Object mi : m) {
            System.out.print(mi+"\t");
        }
        System.out.println("\n");
    }

    public static void nextFloatExample() {
        LinkedList m = new LinkedList();
        Random generator = new Random();

        for(int i=0; i<11; i++) {
            m.add(generator.nextFloat());
        }

        for (Object mi : m) {
            System.out.print(mi+"\t");
        }
        System.out.println("\n");
    }

    public static void nextIntExample() {
        LinkedList m = new LinkedList();
        Random generator = new Random();

        for(int i=0; i<11; i++) {
            m.add(generator.nextInt());
        }

        for (Object mi : m) {
            System.out.print(mi+"\t");
        }
        System.out.println("\n");
    }

    public static void nextBoundIntExample() {
        LinkedList m = new LinkedList();
        Random generator = new Random();

        for(int i=0; i<11; i++) {
            m.add(generator.nextInt(11));
        }

        for (Object mi : m) {
            System.out.print(mi+"\t");
        }
        System.out.println("\n");
    }

    public static void main(String args[]) {
        nextRandomExample();
        nextFloatExample();
        nextIntExample();
        nextBoundIntExample();
    }

}
