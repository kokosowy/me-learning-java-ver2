package ad2017.files.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class CopyFiles {

    public static void main(String[] args) throws IOException {
        Files.copy(Paths.get("pliki/plik.txt"), Paths.get("pliki/CreateDirTest/plik.txt"), StandardCopyOption.REPLACE_EXISTING);
    }

}
