package ad2017.files.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class MoveFiles {
    public static void main(String[] args) throws IOException {
        Files.move(Paths.get("pliki/createDirTest/ruchomyplik.txt"), Paths.get("pliki/ruchomyplik.txt"), StandardCopyOption.REPLACE_EXISTING);
        Files.move(Paths.get("pliki/ruchomytest.txt"), Paths.get("pliki/createDirTest/ruchomyplik.txt"), StandardCopyOption.REPLACE_EXISTING);
    }
}
