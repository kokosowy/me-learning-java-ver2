package ad2017.files.example;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ReadAllLines {

    public static void main(String[] args) throws IOException {
        Files.readAllLines(Paths.get("pliki/plik.txt"), Charset.forName("UTF-8"));
    }

}
