package ad2017.files.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by konradal on 18/07/2017.
 */
public class FilesCreateNew {

    public static void main(String args[]) throws IOException {
        Files.createFile(Paths.get("pliki/plikB.txt"));
    }

}
