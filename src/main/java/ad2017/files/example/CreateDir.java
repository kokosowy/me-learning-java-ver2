package ad2017.files.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by konradal on 18/07/2017.
 */
public class CreateDir {

    public static void main(String[] args) throws IOException {
            Files.createDirectory(Paths.get("pliki/createDirTest"));
    }

}
