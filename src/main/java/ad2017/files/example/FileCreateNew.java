package ad2017.files.example;

import java.io.File;
import java.io.IOException;

/**
 * Created by kokos on 2017-07-17.
 */
public class FileCreateNew {

    public static void main(String[] args) throws IOException {
        File file = new File("pliki/plik.txt");
        if(!file.exists()) {
            file.createNewFile();
        }
    }

}
