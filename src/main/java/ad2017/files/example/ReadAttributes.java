package ad2017.files.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.AclEntry;
import java.nio.file.attribute.AclFileAttributeView;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

/**
 * Created by konradal on 18/07/2017.
 */
public class ReadAttributes {

    public static void main(String[] args) throws IOException {
        Files.createFile(Paths.get("pliki/plik.txt"));
        BasicFileAttributeView attrView = Files.getFileAttributeView(Paths.get("pliki/plik.txt"), BasicFileAttributeView.class);
        BasicFileAttributes attributes = attrView.readAttributes();
        System.out.println(attributes.size()); //rozmiar w bajtach
        System.out.println(attributes.lastModifiedTime()); //ostatnia modyfikacja

        AclFileAttributeView aclAttrView = Files.getFileAttributeView(Paths.get("pliki/plik.txt"), AclFileAttributeView.class);
        List<AclEntry> acl = aclAttrView.getAcl();
        for (AclEntry aclEntry : acl) {
            aclEntry.flags().forEach(System.out::println);
            aclEntry.permissions().forEach(System.out::println);
            System.out.println(aclEntry.principal().getName());
        }
    }

}
