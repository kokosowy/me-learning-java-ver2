package ad2017;

import java.util.Objects;

/**
 * Created by kokos on 2017-05-23.
 * Javastart.pl
 */

public class ObjectClass {

    public boolean compareObjects(Object o1, Object o2) {
        return Objects.equals(o1, o2); //lepsze porownywanie; odporne na NullPointerException
    }

    public void safePrint(Object o) {
        System.out.println(Objects.toString(o)); //lepsze drukowanie; odporne na NullPointerException
    }

    public static void main(String[] args) {

        new ObjectClass().compareObjects("True", "True");

        new ObjectClass().safePrint("Print");

    }
}
