package whats.my.age.again;

public class Options {

    static final String OPTION1 = "Antoni (born 17.08.2017)";
    static final String OPTION2 = "Custom (provide date of birth in format dd.mm.yyyy)";
    static final String OPTION0 = "EXIT";

    public void showOptions() {
        System.out.println("The options are:");
        System.out.println("1) "+ OPTION1);
        System.out.println("2) "+ OPTION2);
        System.out.println("else) "+ OPTION0+"\n");
    }
}