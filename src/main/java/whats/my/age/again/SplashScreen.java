package whats.my.age.again;

import whats.my.age.again.Options;

import java.util.Scanner;

public class SplashScreen {

    private void printSelection(int selection) {
        System.out.println();
        switch (selection) {
            case 1:
                System.out.println("You chose "+ Options.OPTION1+". Let's do this...");
                break;
            case 2:
                System.out.println("You chose "+Options.OPTION2+". Ok, you asked for it...");
                break;
            default:
                System.out.println("You chose to leave."+"\n");
                break;
        }
    }

    public int greetAndSelect() {
        new Options().showOptions();
        System.out.print("What do you choose? ...");

        Scanner input = new Scanner(System.in);
        int selection = input.nextInt();
        printSelection(selection);
        return selection;
    }
}