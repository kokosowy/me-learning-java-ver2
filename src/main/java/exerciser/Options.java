package exerciser;

public class Options {

    static final String OPTION1 = "nice and quick [20 times 2s every 2s]";
    static final String OPTION2 = "strong and long [15 times 4s every 4s]";
    static final String OPTION3 = "suit and tie [custom made for your needs]";
    static final String OPTION0 = "EXIT";

    public void showOptions() {
        System.out.println("The options are:");
        System.out.println("1) "+ OPTION1);
        System.out.println("2) "+ OPTION2);
        System.out.println("3) "+ OPTION3+"\n");
        System.out.println("else) "+ OPTION0+"\n");
    }

}