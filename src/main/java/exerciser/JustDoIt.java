package exerciser;

import java.util.concurrent.TimeUnit;

public class JustDoIt {

    private final String ACTION = "Do it! ";
    private final String REST = "And rest ";

    private void doAction(int duration) throws InterruptedException {
        System.out.print("\t"+ACTION+"\t"+"\t");
        for (int d = 0; d < duration; d++) {
            System.out.print("o");
            TimeUnit.SECONDS.sleep(1);
        }
        System.out.println();
    }

    private void doRest(int rest) throws InterruptedException{
        System.out.print("\t"+REST+"\t");
        for (int r = 0; r < rest; r++) {
            System.out.print("-");
            TimeUnit.SECONDS.sleep(1);
        }
        System.out.println("\n");
    }

    public void letsGo(int amount, int duration, int rest) throws InterruptedException {
        System.out.println();
        System.out.println("Ready?"+"\n");
        TimeUnit.SECONDS.sleep(1);
        System.out.println("Go!"+"\n");
        TimeUnit.SECONDS.sleep(1);
        for (int a = 0; a < amount; a++) {
            System.out.print((a + 1)+")");
            doAction(duration);
            doRest(rest);
        }
    }

}