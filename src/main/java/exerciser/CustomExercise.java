package exerciser;

import java.util.Scanner;

public class CustomExercise {

    Scanner input = new Scanner(System.in);

    private int getAmount() {
        System.out.print("\n"+"Provide amount of intervals: ");
        return input.nextInt();
    }

    private int getDuration() {
        System.out.print("Provide duration of single interval [s]: ");
        return input.nextInt();
    }

    private int getRest() {
        System.out.print("Provide rest time between intervals [s]: ");
        return input.nextInt();
    }

    public void askAndGo() throws InterruptedException {
        new JustDoIt().letsGo(getAmount(),getDuration(),getRest());

    }
}