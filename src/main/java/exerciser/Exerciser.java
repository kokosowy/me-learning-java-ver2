package exerciser;

public class Exerciser {

    public static void main(String[] args) throws InterruptedException {

        JustDoIt justDoIt = new JustDoIt();

        switch (new SplashScreen().greetAndSelect()) {
            case 1:
                justDoIt.letsGo(20, 2, 2);
                break;
            case 2:
                justDoIt.letsGo(15, 4, 4);
                break;
            case 3:
                new CustomExercise().askAndGo();
                break;
            default:
                System.out.println("See you later.");
                break;
        }

    }

}